import { IonicModule } from '@ionic/angular';
import { IdSplitView } from './idsplitview.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularSplitModule } from 'angular-split';
@NgModule({
  declarations: [IdSplitView],
  imports: [
    AngularSplitModule.forRoot(),
    CommonModule, 
    IonicModule
  ],
  exports: [IdSplitView]
})
export class IdSplitViewModule {}
