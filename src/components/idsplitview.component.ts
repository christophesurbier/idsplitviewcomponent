import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ChangeDetectorRef,
  HostListener
} from '@angular/core';

@Component({
  selector: 'idsplitview',
  templateUrl: './idsplitview.component.html',
  styleUrls: ['./idsplitview.component.scss'],
})
export class IdSplitView implements OnInit {
  initialMasterWidth = 30;
  initialDetailWidth = 70;

  @Input() widthMaster = 30;
  @Input() widthDetail = 70;

  /*
  @Output() detailSelected: EventEmitter<{
    idSelected: any;
  }> = new EventEmitter();

  idSelected: any;
*/
  @HostListener('window:resize', ['$event'])
  
  onResize(event) {
    console.log("===== Resize window "+ window.innerWidth);
  }

  constructor(private change: ChangeDetectorRef) {}

  ngOnInit() {}

/*
  selected(param?: any) {
    this.idSelected = param;
    this.detailSelected.emit(this);
  }
*/
}
