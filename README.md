# IDSplitView Component

[![N|iDevotion](https://i1.wp.com/www.hophopfood.org/wp-content/uploads/2018/10/idevotion.jpg)](https://www.idevotion.fr)
**IdSplitViewComponent** is a Ionic 4 component which emulates the UISplitViewController on iOS. It's helps implementing a "master-detail" style app. Something like this: 
![N|iDevotion](https://i.stack.imgur.com/Sfo6l.png)

Meaning on desktop or mobile landscape, you will have a master view on the left side and a detail view on the right side, whereas on mobile portrait, the detail view will be hidden and only the master view will be showed.

- syntax:
  ```
    <idsplitview>
        <div master>
            <!-- Your master view html code-->
        </div>

        <div detail>
            <!-- Your detail view html code-->
        </div>
    </idsplitview>
    ```