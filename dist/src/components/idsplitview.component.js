var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ChangeDetectorRef, HostListener } from '@angular/core';
var IdSplitView = /** @class */ (function () {
    function IdSplitView(change) {
        this.change = change;
        this.initialMasterWidth = 30;
        this.initialDetailWidth = 70;
        this.widthMaster = 30;
        this.widthDetail = 70;
    }
    /*
    @Output() detailSelected: EventEmitter<{
      idSelected: any;
    }> = new EventEmitter();
  
    idSelected: any;
  */
    IdSplitView.prototype.onResize = function (event) {
        console.log("===== Resize window " + window.innerWidth);
    };
    IdSplitView.prototype.ngOnInit = function () { };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], IdSplitView.prototype, "widthMaster", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], IdSplitView.prototype, "widthDetail", void 0);
    __decorate([
        HostListener('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], IdSplitView.prototype, "onResize", null);
    IdSplitView = __decorate([
        Component({
            selector: 'idsplitview',
            templateUrl: './idsplitview.component.html',
            styleUrls: ['./idsplitview.component.scss'],
        }),
        __metadata("design:paramtypes", [ChangeDetectorRef])
    ], IdSplitView);
    return IdSplitView;
}());
export { IdSplitView };
//# sourceMappingURL=idsplitview.component.js.map