var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { IonicModule } from '@ionic/angular';
import { IdSplitView } from './idsplitview.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularSplitModule } from 'angular-split';
var IdSplitViewModule = /** @class */ (function () {
    function IdSplitViewModule() {
    }
    IdSplitViewModule = __decorate([
        NgModule({
            declarations: [IdSplitView],
            imports: [
                AngularSplitModule.forRoot(),
                CommonModule,
                IonicModule
            ],
            exports: [IdSplitView]
        })
    ], IdSplitViewModule);
    return IdSplitViewModule;
}());
export { IdSplitViewModule };
//# sourceMappingURL=idsplitview.module.js.map