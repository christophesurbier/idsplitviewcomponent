import { OnInit, ChangeDetectorRef } from '@angular/core';
export declare class IdSplitView implements OnInit {
    private change;
    initialMasterWidth: number;
    initialDetailWidth: number;
    widthMaster: number;
    widthDetail: number;
    onResize(event: any): void;
    constructor(change: ChangeDetectorRef);
    ngOnInit(): void;
}
